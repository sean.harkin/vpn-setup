package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func main() {

	// keyPassphrase := generatePassphrase(8)
	//publicIP := getPublicIP()
	// replaceENV(keyPassphrase)
	runCommand("chmod +x ./openvpn-install.sh")
	runCommand("sudo APPROVE_INSTALL=y ./openvpn-install.sh")

	uploadCredsFile()

}

func runCommand(command string) {

	args := strings.Split(command, " ")
	cmd := exec.Command(args[0], args[1:]...)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	err := cmd.Run()

	if err != nil {
		log.Panic(err)
	}
}

func getPublicIPAWS() string {
	resp, err := http.Get("http://169.254.169.254/latest/meta-data/public-ipv4")
	if err != nil {
		log.Panic(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Panic(err)
	}

	return string(body)
}

func getPublicIP() string {
	command := "dig +short myip.opendns.com @resolver1.opendns.com"
	args := strings.Split(command, " ")
	cmd := exec.Command(args[0], args[1:]...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Panic(err)
	}
	return string(out)
}

func generatePassphrase(size int) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, size)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func replaceENV(keyPassphrase string) {
	input, err := ioutil.ReadFile("default.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	output := bytes.Replace(input, []byte("${keyPassphrase}"), []byte(keyPassphrase), -1)

	if err = ioutil.WriteFile("env.txt", output, 0666); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func uploadCredsFile() {
	os.Setenv("AWS_ACCESS_KEY_ID", "AKIAY6WXKGHG3MD3NWWP")
	os.Setenv("AWS_SECRET_ACCESS_KEY", "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")
	os.Setenv("AWS_DEFAULT_REGION", "eu-west-1")

	conf := aws.Config{Region: aws.String("eu-west-1")}
	sess := session.Must(session.NewSession(&conf))

	uploader := s3manager.NewUploader(sess)

	file, err := os.Open("/home/clientname.ovpn")
	if err != nil {
		log.Panic("failed to open file")
	}

	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String("open-vpn-creds"),
		Key:    aws.String("clientname.ovpn"),
		Body:   file,
	})

	if err != nil {
		fmt.Println(err)
		log.Panic("Unable to upload")
	}

	fmt.Printf("file uploaded!!, %s", aws.StringValue(&result.Location))
}
